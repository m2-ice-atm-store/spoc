import React from 'react';
import { connect } from 'react-redux';

import Header from '../Board/components/Header';
import Menu from '../Board/components/Menu';
import Content from '../Board/components/Content';

import PageProfiles from './components/PageProfiles';
import PageApplications from './components/PageApplications';

import {
  PAGE_CHANGE_CURRENT_PAGE
} from '../../constants/ActionTypes';

import './style.scss';

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apps: {
        'Profils': <PageProfiles/>,
        'Applications': <PageApplications/>,
      },
      selectedApp: 'Profils',
    };
  }

  componentDidMount() {
    if (Object.keys(this.state.apps).length) {
      this.props.changeCurrentPage(Object.keys(this.state.apps)[0]);
    }
  }

  render() {
    return (
      <div className="root">
        <Header/>
        <Menu
          apps={this.state.apps}
          setSelected={app => this.props.changeCurrentPage(app)}
        />
        <Content>{this.state.apps[this.props.currentPage]}</Content>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentPage: state.page.currentPage,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    changeCurrentPage: page => dispatch({
      type: PAGE_CHANGE_CURRENT_PAGE,
      page
    }),
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin);
