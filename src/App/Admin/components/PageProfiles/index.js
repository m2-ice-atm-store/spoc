import React from 'react';

import ListContainer from '../ListContainer';

import './style.scss';

class PageProfiles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      airports: [
        { text: "LFBO" },
        { text: "LFPG" },
        { text: "LFTS" }
      ],
      profiles: [
        { text: "Controler" },
        { text: "Dataprovider" },
        { text: "Admin" }
      ],
      apps: [
        { text: "app1" },
        { text: "app2" },
        { text: "app3" },
        { text: "app4" }
      ],
      appsComplete: [
        { text: "app1" },
        { text: "app2" },
        { text: "app3" },
        { text: "app4" },
        { text: "app5" }
      ],
      properties: [
        { text: "key", value: "of house" },
        { text: "token", value: "tik tok" },
        { text: "login", value: "log" },
        { text: "password", value: "abcs" }
      ]
    }
  }

  closeEvent(name, key) {
    let newTable = this.state[name];
    newTable.splice(key, 1);
    this.setState(newTable);
  }

  selectEvent(name, key) {
    console.log(this.state[name][key]);
  }

  changeValue(name, key, value) {
    let newTable = this.state[name];
    newTable[key].value = value;
    this.setState(newTable);
  }

  addToList(name, value) {
    let newTable = this.state[name];
    newTable.push(value);
    this.setState(newTable);
  }

  render() {

    return (
      <div>
        <ListContainer
          title="Aéroports"
          list={this.state.airports}
          closeEvent={key => this.closeEvent('airports', key)}
          selectEvent={key => this.selectEvent('airports', key)}
          footer="input"
        />
        <ListContainer
          title="Profils"
          list={this.state.profiles}
          closeEvent={key => this.closeEvent('profiles', key)}
          selectEvent={key => this.selectEvent('profiles', key)}
          footer="input"
        />
        <ListContainer
          title="Applications"
          list={this.state.apps}
          listComplete={this.state.appsComplete}
          selectEvent={key => this.selectEvent('apps', key)}
          hideClose
          footer="select"
        />
        <ListContainer
          title="Propriétés"
          list={this.state.properties}
          editable
          changeValue={(key, value) => this.changeValue('properties', key, value)}
        />
      </div>
    )
  }
}

export default PageProfiles;