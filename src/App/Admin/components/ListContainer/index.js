import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';

import ListItem from '../ListItem';
import ListItemEditable from '../ListItemEditable';

import './style.scss';

class ListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      inputValue: null,
    }
  }

  listUnused() {
    return this.props.listComplete.filter(
      val => !this.props.list.includes(val)
    )
  }

  handleFooterChange(value) {
    console.log(value);
    this.setState({
      selected: value
    })
  }

  changeInputValue(value) {
    console.log(value);
    this.setState({
      inputValue: value
    })
  }
  
  listMap(hideClose) {
    return this.props.list.map((elem, key) =>
      <ListItem
        key={elem.text}
        text={elem.text}
        closeEvent={() => this.props.closeEvent(key)}
        selectEvent={() => this.props.selectEvent(key)}
        hideClose={hideClose}
      />
    )
  }
  listEditableMap() {
    return this.props.list.map((elem, key) =>
      <ListItemEditable
        key={elem.text}
        text={elem.text}
        value={elem.value}
        changeValue={value => this.props.changeValue(key, value)}
      />
    )
  }

  elemListMap() {
    return this.listUnused().map((elem, key) =>
      <MenuItem value={elem.text}>{elem.text}</MenuItem>
    )
  }

  generateFooter() {
    if (this.props.footer === "select"){
      return (
        <CardContent className="list-container-footer">
          <FormControl className="select-controler">
            <Select
              value={this.state.selected}
              onChange={val => this.handleFooterChange(val)}
              displayEmpty
            >
              {this.elemListMap()}
            </Select>
          </FormControl>
          <IconButton color="primary">
            <AddIcon/>
          </IconButton>
        </CardContent>
      )
    } else if (this.props.footer === "input"){
      return (
        <CardContent className="list-container-footer">
          <FormControl className="input-controler">
            <TextField
              value={this.state.inputValue}
              onChange={e => this.changeInputValue(e.target.value)}
            />
          </FormControl>
          <IconButton color="primary">
            <AddIcon/>
          </IconButton>
        </CardContent>
      )
    } else {
      return null;
    }
  }

  render() {
    return (
      <Card className="list-container">
        <CardHeader
          className="list-container-header"
          title={this.props.title}
        />
        <CardContent>
            {
              this.props.list &&
              (
                this.props.editable
                ? this.listEditableMap()
                : this.listMap(this.props.hideClose)
              )
            }
        </CardContent>
        {this.generateFooter()}
      </Card>
     )
  }
}

export default ListContainer;
