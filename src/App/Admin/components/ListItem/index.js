import React from 'react';
import { withStyles  } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import './style.scss';

const styles = theme => ({
  root: {
    padding: '2px 4px',
    margin: '8px',
    display: 'flex',
    alignItems: 'center',
  },
  text: {
    color: 'rgba(0, 0, 0, 0.38)',
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
});

class ListItem extends React.Component {

  render() {
    const {
      classes,
      text,
      closeEvent, selectEvent,
      hideClose
    } = this.props;

    return (
      <Paper component="form" className={classes.root}>
        <div className={classes.text}>
          { text }
        </div>
        {
          !hideClose &&
          <React.Fragment>
            <Divider className={classes.divider} orientation="vertical" />
            <IconButton
              color="secondary"
              className={classes.iconButton}
              aria-label="close"
              onClick={() => closeEvent()}
            >
              <CloseIcon />
            </IconButton>
          </React.Fragment>
        }
        <Divider className={classes.divider} orientation="vertical" />
        <IconButton
          color="primary"
          className={classes.iconButton}
          aria-label="right"
          onClick={() => selectEvent()}
        >
          <ChevronRightIcon />
        </IconButton>
      </Paper>
    )
  }
}

export default withStyles(styles, { withTheme: true })(ListItem);