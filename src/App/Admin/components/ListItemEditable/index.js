import React from 'react';
import { withStyles  } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';

import './style.scss';

const styles = theme => ({
  root: {
    padding: '2px 4px',
    margin: '8px',
    display: 'flex',
    alignItems: 'center',
  },
  text: {
    color: 'rgba(0, 0, 0, 0.38)',
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 2,
  },
  divider: {
    height: 28,
    margin: 4,
  },
});

class ListItemEditable extends React.Component {

  render() {
    const {
      classes,
      text, value,
      changeValue
    } = this.props;

    return (
      <Paper component="form" className={classes.root}>
        <div className={classes.text}>
          { text }
        </div>
        <Divider className={classes.divider} orientation="vertical" />
        <InputBase
          className={classes.input}
          placeholder={text}
          inputProps={{ 'aria-label': text }}
          value={value}
          onChange={e => changeValue(e.target.value)}
        />
      </Paper>
    )
  }
}

export default withStyles(styles, { withTheme: true })(ListItemEditable);