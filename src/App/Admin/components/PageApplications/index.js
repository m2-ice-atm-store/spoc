import React from 'react';

import ListContainer from '../ListContainer';

import './style.scss';

class PageApplications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apps: [
        { text: "app1" },
        { text: "app2" },
        { text: "app3" },
        { text: "app4" }
      ],
      properties: [
        { text: "key", value: "of house" },
        { text: "token", value: "tik tok" },
        { text: "login", value: "log" },
        { text: "password", value: "abcs" }
      ]
    }
  }

  closeEvent(name, key) {
    let newTable = this.state[name];
    newTable.splice(key, 1);
    this.setState(newTable);
  }

  selectEvent(name, key) {
    console.log(this.state[name][key]);
  }

  changeValue(name, key, value) {
    let newTable = this.state[name];
    newTable[key].value = value;
    this.setState(newTable);
  }

  addToList(name, value) {
    let newTable = this.state[name];
    newTable.push(value);
    this.setState(newTable);
  }

  render() {

    return (
      <div>
        <ListContainer
          title="Applications"
          list={this.state.apps}
          closeEvent={key => this.closeEvent('apps', key)}
          selectEvent={key => this.selectEvent('apps', key)}
          footer="input"
        />
        <ListContainer
          title="Propriétés"
          list={this.state.properties}
          editable
          changeValue={(key, value) => this.changeValue('properties', key, value)}
          footer="input"
        />
      </div>
    )
  }
}

export default PageApplications;