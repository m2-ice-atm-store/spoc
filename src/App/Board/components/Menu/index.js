import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
}));

export default function ClippedDrawer(props) {
  const classes = useStyles();

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{ paper: classes.drawerPaper, }}
    >
      <div className={classes.toolbar}></div>
      <Divider />
      <List>
        {
          props.apps &&
          Object.keys(props.apps).map(text => (
            <ListItem
              key={text}
              button
              onClick={() => props.setSelected(text)}
            >
              <ListItemText primary={text} />
            </ListItem>
          ))
        }
      </List>
    </Drawer>
  );
}
