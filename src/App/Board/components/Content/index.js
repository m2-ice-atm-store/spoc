import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,
}));

const theme = createMuiTheme({
  palette: {
    type: 'light',
  },
});

export default function ClippedDrawer(props) {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {props.children}
      </main>
    </ThemeProvider>
  );
}
