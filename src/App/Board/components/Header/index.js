import React from 'react';
import { connect } from 'react-redux';
import { withStyles  } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Clock from 'react-live-clock';
import { Offline, Online } from "react-detect-offline";
import WifiIcon from '@material-ui/icons/Wifi';
import WifiOffIcon from '@material-ui/icons/WifiOff';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  spoc : {
    flex : 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
});

class Admin extends React.Component {

  render() {
    const {
      classes,
      currentAirport, currentProfile
    } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appBar} >
          <Toolbar>
            <Typography variant="h6" noWrap className={classes.spoc}>
              {`SPOC ${currentAirport || ''} ${currentProfile || ''}`}
            </Typography>
            <Typography variant="h6">
              <Online><WifiIcon/></Online>
              <Offline><WifiOffIcon/></Offline>
              <Clock format={'HH:mm:ss'} ticking={true} />
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentAirport: state.page.currentAirport,
    currentProfile: state.page.currentProfile,
  };
};
export default connect(
  mapStateToProps
)(
  withStyles(
    styles,
    { withTheme: true }
  )(Admin));
