import React from 'react';
import { connect } from 'react-redux';

import Header from './components/Header';
import Menu from './components/Menu';
import Content from './components/Content';

import App1 from '../DummyApps/app-fictive-1';
import App2 from '../DummyApps/app-fictive-2';
import App3 from '../DummyApps/app-fictive-3';
import App4 from '../DummyApps/app-fictive-4';

import {
  PAGE_CHANGE_CURRENT_PAGE,
  PAGE_CHANGE_CURRENT_AIRPORT,
  PAGE_CHANGE_CURRENT_PROFILE,
} from '../../constants/ActionTypes';

import './style.scss';

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apps: {
        'app1': <App1 coche={["A","B"]} />,
        'app2': <App2/>,
        'app3': <App3/>,
        'app4': <App4/>,
      },
    };
  }

  componentDidMount() {
    if (Object.keys(this.state.apps).length) {
      this.props.changeCurrentPage(Object.keys(this.state.apps)[0]);
    }
    this.props.changeCurrentAirport(this.props.match.params.airport);
    this.props.changeCurrentProfile(this.props.match.params.profile);
  }

  render() {
    return (
      <div className="root">
        <Header/>
        <Menu
          apps={this.state.apps}
          setSelected={app => this.props.changeCurrentPage(app)}
        />
        <Content>{this.state.apps[this.props.currentPage]}</Content>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentPage: state.page.currentPage,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    changeCurrentPage: page => dispatch({
      type: PAGE_CHANGE_CURRENT_PAGE,
      page
    }),
    changeCurrentAirport: airport => dispatch({
      type: PAGE_CHANGE_CURRENT_AIRPORT,
      airport
    }),
    changeCurrentProfile: profile => dispatch({
      type: PAGE_CHANGE_CURRENT_PROFILE,
      profile
    }),
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);
