import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './style.scss';
import Board from '../Board';
import BoardNotFound from '../BoardNotFound';
import Admin from '../Admin';

class Routing extends React.Component {

  render() {
    return (
      <Switch>
        <Route path="/admin" exact component={Admin} key={1} />
        <Route path="/:airport/:profile" component={Board} key={2} />
        <Route path="/" component={BoardNotFound} key={3} />
      </Switch>
    )
  }
}

export default Routing;
