
import './app3.css';

var React = require('react');

var URL = "https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=api-temps-reel-tisseo"

class ApiRender extends React.Component {

    constructor (){
        super();
        this.state={
            lestitres: null
        }
    }

    componentDidMount(){
        fetch(URL)
        .then(result=>{
            return result.json();
        }).then(datas=>{
            this.setState({lestitres: datas.parameters.dataset});
        })
    }

    render() {
        return (
            <div className="text">
                {this.state.lestitres}
            </div>
        )
    }
}

export default ApiRender;
