import React from 'react';
import map from './img/map.jpg';


class Map extends React.Component {
    render() {
        return (
            <div className="map">
                <img src={map} alt="Map KC" />
                <div>Heure d'atterissage : {this.props.appconf}</div>
            </div>
        )
    }
}

export default Map;