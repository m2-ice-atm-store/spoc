import React from 'react';

import Header from '../Board/components/Header';
import Menu from '../Board/components/Menu';
import Content from '../Board/components/Content';

import './style.scss';

class BoardNotFound extends React.Component {

  render() {
    return (
      <div className="root">
        <Header/>
        <Menu/>
        <Content>Airport or Profile not found</Content>
      </div>
    )
  }
}

export default BoardNotFound;