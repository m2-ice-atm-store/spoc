import {
  PAGE_CHANGE_CURRENT_PAGE,
  PAGE_CHANGE_CURRENT_AIRPORT,
  PAGE_CHANGE_CURRENT_PROFILE,
  PAGE_CHANGE_LOADING,
} from '../constants/ActionTypes';

const initialState = {
  currentPage: null,
  currentAirport: null,
  currentProfile: null,
  loading: false,
};

const page = (state = initialState, action) => {
  switch (action.type) {
    case PAGE_CHANGE_CURRENT_PAGE:
      return Object.assign({}, state, {
        currentPage: action.page
      });
    case PAGE_CHANGE_CURRENT_AIRPORT:
      return Object.assign({}, state, {
        currentAirport: action.airport
      });
    case PAGE_CHANGE_CURRENT_PROFILE:
      return Object.assign({}, state, {
        currentProfile: action.profile
      });
    case PAGE_CHANGE_LOADING:
      return Object.assign({}, state, {
        loading: action.loading
      });
    default:
      return state;
  }
}

export default page
